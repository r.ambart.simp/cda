<?php

namespace App\Repository;

use App\Entity\Topic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Topic|null find($id, $lockMode = null, $lockVersion = null)
 * @method Topic|null findOneBy(array $criteria, array $orderBy = null)
 * @method Topic[]    findAll()
 * @method Topic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TopicRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Topic::class);
    }
    // /**
    //  * 
    //  * @return Topic[] 
    //  */
    // public function findAll():array {
    //     $topic = [];

    //     $connection = DbConnexion::getConnection();
        
    //     $query = $connection->prepare("SELECT * FROM topic");
    //     $query->execute();
    
    //     $results = $query->fetchAll();
    //     foreach ($results as $line) {
    //         $topic[] = $this->sqlToArticle($line);
    //     }
    //     return $topic;
    // }

    // public function add(Topic $topic): void {
    //     $connection = DbConnexion::getConnection();
     
    //     $query = $connection->prepare("INSERT INTO topic (title) VALUES (:title)");
   
    //     $query->bindValue(":title", $topic->title);
    //     $query->execute();

    
    //     $topic->id = $connection->lastInsertId();
    // }


    // public function update(Topic $topic): void {
    //     $connection = DbConnexion::getConnection();

    //     $query = $connection->prepare("UPDATE topic SET title=:title WHERE id=:id");
    //     $query->bindValue(":title", $topic->title);

    //     $query->execute();

    // }

    // public function remove(Topic $topic): void {
    //     $connection = DbConnexion::getConnection();

    //     $query = $connection->prepare("DELETE FROM topic WHERE id=:id");
    //     $query->bindValue(":id", $topic->id, \PDO::PARAM_INT);

    //     $query->execute();
    // }

    
}


