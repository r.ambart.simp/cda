<?php

namespace App\Controller;

use App\Entity\Topic;
use App\Form\TopicType;
use App\Repository\TopicRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

    /**
     * @Route("/api/topic", name="topic")
     */
class TopicController extends AbstractController
{
     /**
     * @var SerializerInterface
     */
    private $serializer;
 
    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
        
    }
    /**
     * @Route(methods="GET")
     */
    public function index(TopicRepository $repo)
    {
        $topic = $repo->findAll();
        
        $json = $this->serializer->serialize($topic, 'json');


        return new JsonResponse($json, 200, [], true);

    }

    /**
     * @Route("/{topic}", methods="GET")
     */
    public function one(Topic $post) {
        return new JsonResponse($this->serializer->serialize($post, 'json'), 200, [], true);
    }

     /**
     * @Route("/add", name="topic")
     */
    public function add(Request $request, EntityManagerInterface $manager) {
        $topic = new Topic();     
        $form = $this->createForm(TopicType::class, $topic);

        $form->submit(json_decode($request->getContent(), true));

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($topic);
            $manager->flush();

            return new JsonResponse($this->serializer->serialize($topic, 'json'), 201, [], true);
        }

        return $this->json($form->getErrors(true), 400);

    }

    /**
     * @Route("/update/{id}", methods="PATCH")
     */
    public function PatchTopic(Topic $topic, Request $request, EntityManagerInterface $manager) {
       

        $form = $this->createForm(TopicType::class, $topic);

        $form->submit(json_decode($request->getContent(), true), false);

        if($form->isSubmitted() && $form->isValid()) {

            $manager->flush();

            return new JsonResponse($this->serializer->serialize($topic, 'json'), 200, [], true);
        }

        return $this->json($form->getErrors(true), 400);
    
}

 /**
     * @Route("/remove/{id}", methods="DELETE")
     */
    public function RemoveTopic(Topic $topic, EntityManagerInterface $manager){

        $manager->remove($topic);
        $manager->flush();

        return new JsonResponse($this->serializer->serialize($topic, 'json'), 200, [], true);

    }


}
