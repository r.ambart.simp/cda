<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\Topic;
use App\Form\PostType;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
     * @Route("/api/topic{id}/posts", name="post")
     */
class PostController extends AbstractController
{
     /**
     * @var SerializerInterface
     */
    private $serializer;
 
    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }
    /**
     * @Route(methods="GET")
     */
    public function index(PostRepository $repo)
    {
        $post = $repo->findAll();
        $json = $this->serializer->serialize($post, 'json');

        return new JsonResponse($json, 200, [], true);

    }

    /**
     * @Route("/add", name="post")
     */
    public function add(Request $request, EntityManagerInterface $manager) {
        $post = new Post();     
        $form = $this->createForm(PostType::class, $post);

        $form->submit(json_decode($request->getContent(), true));

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($post);
            $manager->flush();

            return new JsonResponse($this->serializer->serialize($post, 'json'), 201, [], true);
        }

        return $this->json($form->getErrors(true), 400);
    }

}

